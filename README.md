# Gitlab CSV Import

Create gitlab issues from a CSV.

Status: Import via console

`node app.js {gitlab access token} {project id} {semi comma seperated issue titles,weight}`

Gitlab access token can be generated here https://gitlab.com/profile/personal_access_tokens

Project ID you find below the project title on the detail page. i.e. https://gitlab.com/stanwood/apps/wun/wunderdeals-api

**Example**

`node app.js 3z***6z 8582863 "I love cats,2; I love dogs,3"`

// Documentation for issue creation
// https://docs.gitlab.com/ee/api/issues.html#new-issue
