var request = require('request');

var gitlabAccessToken = process.argv[2];
var projectKey = process.argv[3];
var titlesString = process.argv[4];
var url = "https://gitlab.com/api/v4/projects/" + projectKey + "/issues";
var headers = {
	"PRIVATE-TOKEN": gitlabAccessToken
};


var main = function () {


	if (!gitlabAccessToken) {
		console.error("Missing gitlab access token");
		return;
	}
	if (!projectKey) {
		console.error("Missing gitlab project key");
		return;
	}

	if (!titlesString) {
		console.error("Missing titles string");
		return;
	}

	var issues = titlesString.split(";");

	if (issues.length == 0) {
		console.error("No issues founds");
		return;
	}

	for (var i = 0; i < issues.length; i++) {

		var issue = issues[i];
		var properties = issue.split(",");

		var title = properties[0];
		var weight = properties[1];

		if (!title ) {
			console.error("Missing title");
					return;
		}
		if (!weight ) {
			console.error("Missing weight");
			return;
		}

		var fullUrl = url + "?title=" + title + "&weight=" + weight;
		console.log (fullUrl);

		request.post({
				url: fullUrl,
				headers: headers
			},
			function (error, response, body) {
				if (response && response.statusCode == 200) {
					console.log(body);
				} else {
					console.log('error:', error);
					console.log('statusCode:', response && response.statusCode);
					console.error('response body: ', body);
				}
			});
	}
}

main();